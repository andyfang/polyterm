## A modern and hackable terminal IDE

![Polyterm screenshot](http://puu.sh/nUMPj/d1ff1cfb46.png)

### Why replace my old terminal?
Your terminal isn't a very good text editor, and text editors aren't terminals. Polyterm is an IDE in the world of terminals.

### Todo
- Split pane &amp; multiple tabs
- Autocomplete commands
- Customize your terminal with CSS
- Multiple shell support (Zsh, bash, etc)
- Gitter community channel

### Development

1. Install dependencies `npm install`
2. Run unpackaged `npm run dev`
3. Compile the standalone `npm package`

### Technologies
- [ReactJS](https://facebook.github.io/react/docs/getting-started.html)
- [Electron](http://electron.atom.io/)
- [Redux](http://redux.js.org/)

[Why we use a custom shell (how terminal emulators work)](http://www.linusakesson.net/programming/tty/)
