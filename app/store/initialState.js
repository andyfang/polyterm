const initialState = {
  projects: [
    {
      name: 'New project',
      style: {
        'backgroundColor': '#d61a7f'
      },
      commands: [],
      cmd: null,
      active: true,
      paneRows: [
        [{
          type: 'editor',
          width: '100%',
          height: '20%'
        }, {
          type: 'terminal',
          width: '100%',
          height: '20%'
        }],
        [{
          type: 'terminal',
          width: '100%',
          height: '80%'
        }]
      ]
    }
  ]
};

export default initialState;
