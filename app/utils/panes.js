import Split from 'split.js';

export function splitVertical(nodes) {
  const numRows = nodes.length;
  let splittedNodes = [];

  for (let i = 1; i <= numRows; i++) {
    splittedNodes.push(`.workspace .row:nth-child(${i})`);
  }

  Split(splittedNodes, {
    sizes: new Array(numRows).fill(100 / numRows),
    direction: 'vertical',
    gutterSize: 10,
    minSize: 200
  });
}
export function splitHorizontal(nodes) {
  for (let row of nodes) {
    let splittedNodes = [];
    for (let column of Array.from(row.children)) {
      splittedNodes.push(column);
    }
    const numColumns = splittedNodes.length;
    if (splittedNodes.length > 1) {
      console.log('splittedNodes: ', splittedNodes);
      Split(splittedNodes, {
        sizes: new Array(numColumns).fill(100 / numColumns),
        direction: 'horizontal',
        gutterSize: 10,
        minSize: 200
      });
    }
  }

}
