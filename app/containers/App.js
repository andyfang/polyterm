import React, { Component, PropTypes } from 'react';
import { Provider } from 'react-redux';
import configureStore from '../store/configureStore';
import initialState from '../store/initialState';

export default class App extends Component {
  static propTypes = {
    children: PropTypes.element.isRequired
  }

  render() {
    return (
      <Provider store={configureStore(initialState)}>
        <div>
          {this.props.children}
          {
            (() => {
              if (process.env.NODE_ENV !== 'production') {
                const DevTools = require('./DevTools');
                  return <DevTools />;
              }
            })()
          }
        </div>
      </Provider>
    )
  }
}
