import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect }  from 'react-redux';
import * as actions from '../actions';
import Terminal from '../components/Terminal';
import Editor from '../components/Editor';
import Sidebar from '../components/Sidebar';
import PaneRow from '../components/PaneRow';
import {
  splitVertical,
  splitHorizontal
} from '../utils/panes';
import '../app.global.less';

export default class Home extends Component {
  constructor(props) {
    super(props);

    this.updateDimensions = this.updateDimensions.bind(this);
    this.activeProject = this.props.projects
      .filter(project => project.active)[0];
    this.state = {
      activeProject: this.activeProject
    }
  }
  updateDimensions() {
    this.refs.workspace.style.width = `${window.innerWidth - this.refs.sidebar.refs.sidebar.clientWidth}px`;

    const { projects } = this.props;
    const rowNodes = Array.from(this.refs.workspace.childNodes).filter(node => {
      if (node.className === 'row') return node;
    });

    splitVertical(rowNodes);
    splitHorizontal(rowNodes);

  }
  componentDidMount(props) {
    this.updateDimensions();
    console.log('home props;', this.props);
    window.addEventListener('resize', this.updateDimensions);
    window.addEventListener('webkitfullscreenchange', this.updateDimensions);

  }
  render() {

    const panes = this.state.activeProject.paneRows.map((row, i) => {
      return <PaneRow panes={row} key={i} activeProject={this.state.activeProject} />;
    });

    const projectActions = {
      addProject: actions.addProject
    }

    return (
      <div className="main">
        <Sidebar ref="sidebar" projects={this.props.projects} projectActions={projectActions}/>
        <div className="workspace" ref="workspace">
          { panes }
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    projects: state.projects
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
