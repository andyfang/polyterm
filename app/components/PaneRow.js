import React, { Component } from 'react';
import Terminal from '../components/Terminal';
import Editor from '../components/Editor';

export default class PaneRow extends Component {
  constructor(props) {
    super(props);
    console.log('panes: ', this.props.activeProject);
  }
  render() {
    const rows = this.props.panes.map((pane, i) => {
      switch (pane.type) {
        case 'terminal':
          return (<Terminal key={i} activeProject={this.props.activeProject}/>);
        case 'editor':
          return (<Editor key={i} />);
      }
    });

    return (
      <div className="row">
        { rows }
      </div>
    )
  }
}
