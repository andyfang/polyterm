import React, { Component } from 'react';

export default class Field extends Component {
  render() {
    const status = this.props.error ?
      <span className="octicon octicon-x status"></span> :
      <span className="octicon octicon-check status"></span>;

    return (
      <div className="output">
        <div className="line"><label>$ polyterm</label> {this.props.cmd}</div>
        <div className="terminal__msg">{this.props.msg}</div>
      </div>
    )
  }
}
