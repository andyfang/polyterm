import React, { Component } from 'react';

export default class Stdin extends Component {
  render() {
    return (
      <form onSubmit={this.props.onSubmit} className="stdin">
        <label>$ polyterm</label>
        <input type="text" name="stdin"/>
      </form>
    )
  }
}
