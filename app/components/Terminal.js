import React, { Component, PropTypes } from 'react';
import Stdin from '../components/Stdin';
import Field from '../components/Field';
import Terminus from '../utils/terminus';
import classNames from 'classnames';

export default class Terminal extends Component {
  constructor(props) {
    super(props);
    this.focusStdin = this.focusStdin.bind(this);
    this.execCommand = this.execCommand.bind(this);
    this.state = {
      focused: false
    };
  }
  componentDidMount() {
    console.log('terminal props', this.props);
    this.refs.stdin.addEventListener('focus', () => {
      this.setState({
        focused: true
      });
    });
    this.refs.stdin.addEventListener('blur', () => {
      this.setState({
        focused: false
      });
    });
  }
  execCommand(ev) {
    ev.preventDefault();
    const cmd = ev.target.stdin.value;
    const { terminalActions } = this.props;
    // const terminal = new Terminus();
    //
    // ev.target.stdin.value = '';
    // terminal.send(cmd);
    console.log(terminalActions);

  }
  focusStdin() {
    const $stdin = this.refs.stdin;
    $stdin.focus();
  }
  componentDidUpdate() {
    this.refs.stdin.scrollIntoView({ behavior: 'smooth' });
  }
  render() {

    // const stdout = state.projects[0].commands.map((stdout, i) => {
    //   console.log('rendering field');
    //   return (
    //     <Field msg={stdout.msg} cmd={stdout.cmd} key={i}/>
    //   )
    // });


    const terminalClass = classNames({
      'terminal': !this.state.focused,
      'terminal terminal--focused': this.state.focused
    });

    return (
      <div className={terminalClass} onClick={this.focusStdin} ref="terminal">
        {/*{stdout}*/}
        <form onSubmit={this.execCommand} className="terminal__stdin">
          <label className="terminal__label" htmlFor="terminal__stdin">$</label>
          <input type="text" className="terminal__stdin" ref="stdin"/>
        </form>
      </div>
    )
  }
}
