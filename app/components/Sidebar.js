import React, { Component } from 'react';
import watch from 'watch';
import dirTree from 'directory-tree';
import remote from 'remote';
import { dialog } from 'remote'; // electron dialogs are only available in the main process. In order to use it in the renderer process you need to require from remote.
import { Treebeard } from 'react-treebeard';
import className from 'classnames';

export default class Sidebar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tree: {},
      unfilteredTree: {} // update only
    }
    this.addProject = this.addProject.bind(this);
  }
  addProject() {

    const { addProject } = this.props.projectActions;

    function openDialog(options) {
      return new Promise((resolve, reject) => {
        dialog.showOpenDialog(options, selected => {
          if (!selected) {
            reject(selected);
          } else {
            resolve(selected.toString());
          }
        });
      });
    }

    function watchFiles(dir) {
      watch.createMonitor(dir, monitored => {
        console.log(`Started watcher on ${dir}`);

        monitored.on('created', (f, stat) => {
          console.log('Created new file');
        });
        monitored.on('changed', (f, curr, prev) => {
          console.log(f);
        });
        monitored.on('removed', (f, stat) => {
          console.log('Removed file');
        });
      });
    }

    // Call promises in order
    openDialog({
        properties: ['openDirectory']
      })
      .then(dir => {
        const projectName = dir.toString();

        watchFiles.bind(this, projectName);
        addProject(projectName);
        return dirTree.directoryTree(dir);
      })
      .then(projectTree => {
        this.setState({
          tree: projectTree,
          unfilteredTree: projectTree
        });
      })
  }
  removeProject() {
    monitored.stop();
  }
  render() {
    const projectIcons = this.props.projects.map((project, i) => {
      const projectInitials = project.name.replace(/[^A-Z]/g, '');
      const projectIconClass = className({
        'sidebar__project sidebar__project--active': project.active,
        'sidebar__project': !project.active
      });
      return (
        <li className={projectIconClass} style={project.style} key={i}>{projectInitials}</li>
      )
    });

    return (
      <div className="sidebar" ref="sidebar">
        <ul className="sidebar__projects" ref="projectBar">
          { projectIcons }
          <li className="sidebar__project sidebar__project--add octicon octicon-plus" onClick={this.addProject}></li>
        </ul>
      </div>
    )
  }
}
