import React, { Component, PropTypes } from 'react';
import Codemirror from 'react-codemirror';
import 'codemirror/mode/javascript/javascript';

export default class Editor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      code: ''
    }
    this.updateCode = this.updateCode.bind(this);
  }
  updateCode(newCode) {
    this.setState({
      code: newCode
    });
  }
  render() {
    const editorOptions = {
      lineNumbers: true,
      mode: 'javascript',
      theme: 'dracula',
      indentWithTabs: true
    }

    return (
      <Codemirror
        className="editor"
        theme="dracula"
        value={this.state.code}
        onChange={this.updateCode}
        options={editorOptions}
      />
    );
  }
};
