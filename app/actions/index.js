import * as types from '../constants/actionTypes';

// Terminal Actions
export function execCommand(cmd) {
  return {
    type: types.EXEC_COMMAND,
    cmd
  }
}
// Sidebar Actions
export function addProject(name) {
  return {
    type: types.ADD_PROJECT,
    projects: {
      name
    }
  }
}
