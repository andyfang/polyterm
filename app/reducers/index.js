import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';
import { projectsReducer as projects } from '../reducers/projects';

// Run action through all reducers each time an action is dispatched
const rootReducer = combineReducers({
  routing,
  projects
});

export default rootReducer;
