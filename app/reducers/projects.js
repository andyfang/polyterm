import * as types from '../actions';
import initialState from '../store/initialState';

export function projectsReducer(state = initialState, action) {

  switch (action.type) {
    case types.EXEC_COMMAND:
      return {
        ...state,
        cmd: action.cmd
      };
    case types.ADD_PROJECT:
      return {
        ...state
      }
    default:
      return state;
  }
}
